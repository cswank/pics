package main

import (
	"fmt"
	"net/http"
	"bitbucket.org/cswank/pics/controllers"
	"bitbucket.org/cswank/pics/auth"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/login", auth.Login).Methods("POST")
	r.HandleFunc("/api/logout", auth.Logout).Methods("POST")
	r.HandleFunc("/api/ping", Ping).Methods("GET")
	r.HandleFunc("/api/pics", Upload).Methods("POST")
	r.HandleFunc("/api/pics", GetPics).Methods("GET")
	r.HandleFunc("/api/pics/{id}", GetPic).Methods("GET")
	r.HandleFunc("/api/pics/{id}/{size}", GetThumb).Methods("GET")
	
	http.Handle("/", r)
	fmt.Println("listening on 0.0.0.0:8080")
	http.ListenAndServe(":8080", nil)
}

func Ping(w http.ResponseWriter, r *http.Request) {
	auth.CheckAuth(w, r , controllers.Ping)
}

func Upload(w http.ResponseWriter, r *http.Request) {
	auth.CheckAuth(w, r , controllers.Upload)
}

func GetPics(w http.ResponseWriter, r *http.Request) {
	auth.CheckAuth(w, r , controllers.GetPictures)
}

func GetPic(w http.ResponseWriter, r *http.Request) {
	auth.CheckAuth(w, r , controllers.GetPicture)
}

func GetThumb(w http.ResponseWriter, r *http.Request) {
	auth.CheckAuth(w, r , controllers.GetThumb)
}
