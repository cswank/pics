package controllers

import (
	"net/http"
	"encoding/json"
	"bitbucket.org/cswank/pics/models"
	"gopkg.in/mgo.v2/bson"
	"path"
)

func Ping(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error {
	w.Write([]byte("pong"))
	return nil
}

type picture struct {
	Name string `json:"name"`
	Image string `json:"image"`
	Thumb string  `json:"thumb"`
	Medium string `json:"medium"`
}

func GetPictures(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error {
	pictures, err := models.GetPictures(bson.M{})
	if err != nil {
		return err
	}
	response := make([]picture, len(pictures))
	for i, pic := range pictures {
		response[i] = picture{
			Name: pic.Name,
			Image:  path.Join("/api/pics", pic.Id.Hex()),
			Thumb:  path.Join("/api/pics", pic.Id.Hex(), "thumb"),
			Medium:  path.Join("/api/pics", pic.Id.Hex(), "medium"),
		}
	}
	enc := json.NewEncoder(w)
	return enc.Encode(map[string]interface{}{"pictures": response})
}

func GetPicture(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error {
	return nil
}

func GetThumb(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error {
	id := bson.ObjectIdHex(vars["id"])
	thumb, err := models.GetThumb(id, vars["size"])
	if err == nil {
		w.Write(thumb)
	}
	return err
}



















