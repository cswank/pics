package controllers

import (
	"net/http"
	"bitbucket.org/cswank/pics/models"
)

func Upload(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error {
	f, header, err := r.FormFile("file")
	if err != nil {
		return err
	}
	defer f.Close()
	pic, err := models.NewPicture(f, header.Filename)
	if err != nil {
		return err
	}
	defer pic.Close()
	return pic.Save()
}
