'use strict';

var PicCtrl = function ($scope, $modalInstance, pic) {
    $scope.pic = pic;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};
