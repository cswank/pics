'use strict';

var UploadCtrl = function ($scope, $modalInstance, $pictures) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.onFileSelect = function($files) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            $scope.upload = $pictures.upload(file, function() {
                console.log("success");
            });
        }
    };
};
