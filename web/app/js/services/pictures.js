'use strict';

/* Services */
angular.module('pics.services')
    .factory('$pictures', ['$http', '$upload', function($http, $upload) {
        return {
            get: function(callback, errorCallback) {
                $http({
                    url: '/api/pics',
                    method: "GET",
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data, status, headers, config) {
                    console.log(data);
                    callback(data);
                }).error(function (data, status, headers, config) {
                    errorCallback();
                });
            },
            upload: function(file, callback, errorCallback) {
                $upload.upload({
                    url: '/api/pics', 
                    method: 'POST',
                    //withCredentials: true,
                    file: file,
                    fileName: 'pic'
                }).progress(function(evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function(data, status, headers, config) {
                    // file is uploaded successfully
                    console.log("success", data);
                    callback();
                });
            }
        }
    }]);
