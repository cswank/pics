'use strict';

/* Services */
angular.module('pics.services')
    .factory('$auth', ['$http', function($http) {
        return {
            login: function(username, password, callback, errorCallback) {
                $http({
                    url: '/api/login',
                    method: "POST",
                    data: JSON.stringify({username:username, password: password}),
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data, status, headers, config) {
                    callback();
                }).error(function (data, status, headers, config) {
                    errorCallback();
                });
            },
            logout: function(callback) {
                $http({
                    url: '/api/logout',
                    method: "POST",
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data, status, headers, config) {
                    callback();
                }).error(function (data, status, headers, config) {
                    
                });
            },
            ping: function(callback) {
                $http.get("/api/ping").success(function(data) {
                    callback(data);
                });
            }
        }
    }]);
