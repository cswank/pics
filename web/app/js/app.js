'use strict';

// Declare app level module which depends on filters, and services
angular.module('pics', [
    'ngRoute',
    'pics.filters',
    'pics.services',
    'pics.directives',
    'pics.controllers',
    'ui.bootstrap',
    'ngStorage',
    'angularFileUpload'
]).config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'partials/root.html',
            controller: 'RootCtrl'
        });
    $routeProvider.otherwise({
        redirectTo: '/'
    });
}]);
