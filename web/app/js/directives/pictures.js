'use strict';

/* Directives */
angular.module('pics.directives')
    .directive('pictures', ['$pictures', function($pictures) {
        var url = '/components/pictures.html?x=' + new Date().getTime();
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            templateUrl: url,
            controller: function($scope, $timeout, $modal) {
                $pictures.get(function(data) {
                    $scope.pictures = data.pictures;
                });
                $scope.upload = function() {
                    var dlg = $modal.open({
                        templateUrl: 'components/upload.html',
                        controller: UploadCtrl
                    });
                };
                $scope.getImage = function(pic) {
                    var dlg = $modal.open({
                        templateUrl: 'components/pic.html',
                        controller: PicCtrl,
                        resolve: {
                            pic: function () {
                                return pic;
                            }
                        }
                    });
                };
            }
        }
    }]);
