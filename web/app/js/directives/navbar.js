'use strict';

/* Directives */
angular.module('pics.directives')
    .directive("authNavbar", ['$rootScope', '$auth', '$localStorage', '$modal', function($rootScope, $auth, $localStorage, $modal) {
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            templateUrl: 'components/navbar.html?x=' + new Date().getTime(),
            controller: function($scope, $timeout, $modal) {
                $scope.$storage = $localStorage.$default({
                    username: ""
                });
                $scope.username = "";
                $('[data-hover="dropdown"]').dropdownHover();
                $scope.login = function(errorMessage) {
                    var dlg = $modal.open({
                        template: '<div>  <div class="modal-header">    <a class="close" data-dismiss="modal">×</a>    <h3>Login</h3>    <a class="error">{{message}}</a>  </div>  <div class="modal-body">    <input placeholder="user name" ng-model="user.name" autocapitalize="off" autocorrect="off" auto-focus/><br/>    <input placeholder="password" type="password" ng-model="user.password" ng-enter="ok()"/>  </div>  <div class="modal-footer">    <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>    <button type="button" class="btn btn-primary" ng-enter="ok()" ng-click="ok()">Ok</button>  </div></div>',
                        controller: LoginCtrl,
                        resolve: {
                            message: function () {
                                return errorMessage;
                            }
                        }
                    });
                    dlg.result.then(function(user) {
                        $scope.username = user.name;
                        $scope.$storage.username = user.name;
                        $scope.password = user.password;
                        $auth.login($scope.username, $scope.password, function(){
                            $scope.loggedIn = true;
                            $rootScope.loggedIn = true;
                        }, function(){
                            $scope.login("username or password not correct, please try again");
                        });
                    });
                }
                
                $scope.logout = function() {
                    $auth.logout(function() {
                        $scope.loggedIn = false;
                    });
                }
                function ping() {
                    $auth.ping(function(data) {
                        $scope.loggedIn = true;
                        $rootScope.loggedIn = true;
                    }, function() {
                        $scope.loggedIn = false;
                        $rootScope.loggedIn = false;
                        $scope.errMsg = "login failed"
                    });
                }
                $scope.loggedIn = false;
                $rootScope.loggedIn = false;
                ping();
            }
        }
    }]);
