package main

import (
	"fmt"
	"errors"
	"gopkg.in/alecthomas/kingpin.v1"
	"code.google.com/p/gopass"
	"bitbucket.org/cswank/pics/models"
)

var (
	delete = kingpin.Flag("delete", "Delete a user").Short('d').Bool()
	add = kingpin.Flag("add", "Add a user").Short('a').Bool()
	update = kingpin.Flag("update", "Update a user").Short('u').Bool()
)

type Action func(user *models.User)

func main() {
	kingpin.Parse()
	if *add {
		addUser()
	} else if *update {
		updateUser()
	} else if *delete {
		deleteUser()
	} else {
		fmt.Println("no action selected (type zuser --help)")
	}
}

func updateUser() {
	//deleteOrUpdate(doUpdate)
}

func doUpdate(user *models.User) {
	fmt.Println(user)
}

func deleteUser() {
	//deleteOrUpdate(doDelete)
}

func doDelete(user *models.User) {
	err := user.Delete()
	defer user.Close()
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("user deleted")
	}
}

// func deleteOrUpdate(action Action) {
// 	user := getUser()
// 	action(&user)
// }

// func getUser() models.User {
// 	users, err := models.GetUsers()
// 	if err != nil {
// 		panic(err)
// 	}
// 	for i, user := range users {
// 		fmt.Printf("\n%d:  %s", i + 1, user.Email)
// 	}
// 	var x int
// 	fmt.Print("\nselect: ")
// 	fmt.Scanf("%d", &x)
// 	x -= 1
// 	if x >= len(users) || x < 0 {
// 		panic("bad choice")
// 	}
// 	u := users[x]
// 	return u
// }

func addUser() {
	u := &models.User{}
	defer u.Close()
	err := promptUser(u)
	if  err != nil {
		fmt.Println(err.Error())
		return
	}
	err = u.Save()
	if err != nil {
		panic(err)
	}
	fmt.Println("added user")
}

func promptUser(u *models.User) error {
	fmt.Print("usernamel: ")
	fmt.Scanf("%s", &u.Username)
	pw, _ := gopass.GetPass("password: ")
	u.Password, _ = gopass.GetPass("confirm password: ")
	if pw != u.Password {
		return errors.New("passwords don't match")
	}
	return nil
}
