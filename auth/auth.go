package auth

import (
	"log"
	"os"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"io/ioutil"
	"bitbucket.org/cswank/pics/models"
	"github.com/gorilla/securecookie"
)

type controller func(w http.ResponseWriter, r *http.Request, u *models.User, vars map[string]string) error

var (
	hashKey        = []byte(os.Getenv("HASH_KEY"))
	blockKey       = []byte(os.Getenv("BLOCK_KEY"))
	SecureCookie   = securecookie.New(hashKey, blockKey)
)

func CheckAuth(w http.ResponseWriter, r *http.Request, ctrl controller) {
	user, err := getUserFromCookie(r)
	defer user.Close()
	if err == nil && user.IsAuthorized() {
		
		vars := mux.Vars(r)
		err = ctrl(w, r, user, vars)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		http.Error(w, "Not Authorized", http.StatusUnauthorized)
	}
}

func NoAuth(w http.ResponseWriter, r *http.Request, ctrl controller) {
	vars := mux.Vars(r)
	err := ctrl(w, r, nil, vars)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func getUserFromCookie(r *http.Request) (*models.User, error) {
	user := &models.User{}
	cookie, err := r.Cookie("pics")
	if err == nil {
		m := map[string]string{}
		err = SecureCookie.Decode("pics", cookie.Value, &m)
		if err == nil {
			user.Username = m["user"]
		}
	}
	return user, err
}

func Logout(w http.ResponseWriter, r *http.Request) {
	cookie := &http.Cookie{
		Name:  "pics",
		Value: "",
		Path:  "/",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
}

func Login(w http.ResponseWriter, r *http.Request) {
	log.Println("login")
	user := &models.User{}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "bad request 1", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, user)
	if err != nil {
		
		http.Error(w, "bad request 2", http.StatusBadRequest)
		return
	}
	goodPassword, err := user.CheckPassword()
	if !goodPassword {
		log.Println(err)
		http.Error(w, "bad request 3", http.StatusBadRequest)
		return 
	}
	value := map[string]string{
		"user": user.Username,
	}
	encoded, _ := SecureCookie.Encode("pics", value)
	cookie := &http.Cookie{
		Name:  "pics",
		Value: encoded,
		Path:  "/",
		HttpOnly: false,
	}
	http.SetCookie(w, cookie)
}
