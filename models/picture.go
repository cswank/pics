package models

import (
	"bytes"
	"io"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"image/jpeg"
	"image"
	"github.com/disintegration/imaging"
	"fmt"
)

var (
	sizes = map[string]size{
		"thumb": size{100, 100},
		"medium": size{300, 300},
	}
)

type Picture struct {
	src io.Reader
	Id bson.ObjectId `bson:"_id" json:"id"`
	Name string      `bson:"name" json:"name"`
	Thumb  []byte `bson:"thumb"`
	Medium []byte `bson:"medium"`
	db *mgo.Database
	session *mgo.Session
	fs *mgo.GridFS
}

type size struct {
	x int
	y int
}

func NewPicture(f io.Reader, name string) (*Picture, error) {
	p := &Picture{src: f, Name:name}
	return p, p.getDB()
}

func GetPicture(id bson.ObjectId, size string) ([]byte, error) {
	// session, db, err := GetDB()
	// if err != nil {
	// 	return []byte{}, err
	// }
	// defer session.Close()
	return []byte{}, nil
}

func GetThumb(id bson.ObjectId, size string) ([]byte, error) {
	session, db, err := GetDB()
	if err != nil {
		return []byte{}, err
	}
	defer session.Close()
	pic := &Picture{}
	db.C("thumbs").Find(bson.M{"imageId": id}).Select(bson.M{size: 1}).One(pic)
	if size == "medium" {
		return pic.Medium, nil
	}
	return pic.Thumb, nil
}

func GetPictures(query bson.M) ([]Picture, error) {
	p := []Picture{}
	session, db, err := GetDB()
	if err != nil {
		return p, err
	}
	defer session.Close()
	c := db.C("pictures.files")
	err = c.Find(query).All(&p)
	return p, err
}

func (p *Picture) Save() error {
	dst, err := p.fs.Create(p.Name)
	if err != nil {
		return err
	}
	io.Copy(dst, p.src)
	dst.Close()
	return p.saveThumbs()
}

func (p *Picture) saveThumbs() error {
	src, err := p.fs.Open(p.Name)
	if err != nil {
		return err
	}
	img, _, err := image.Decode(src)
	if err != nil {
		return err
	}
	src.Close()
	c := p.db.C("thumbs")
	query := bson.M{
		"imageId": src.Id(),
	}
	for key, val := range sizes {
		data, err := p.getThumb(img, val)
		if err != nil {
			return err
		}
		query[key] = data
	}
	fmt.Println(query)
	return c.Insert(query)
}

func (p *Picture) getThumb(img image.Image, s size) ([]byte, error) {
	t := imaging.Thumbnail(img, s.x, s.y, imaging.CatmullRom)
	thumb := t.SubImage(t.Bounds())
	buf := &bytes.Buffer{}
	jpeg.Encode(buf, thumb, nil)
	data := make([]byte, buf.Len())
	_, err := buf.Read(data)
	return data, err
}

func (p *Picture)getDB() error {
	if p.session != nil {
		return nil
	}
	var err error
	p.session, p.db, err = GetDB()
	if err == nil {
		p.fs = p.db.GridFS("pictures")
	}
	return err
}

func (p *Picture)Close() {
	if p.session != nil {
		p.session.Close()
	}
}
