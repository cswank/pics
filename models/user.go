package models

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"code.google.com/p/go.crypto/bcrypt"
)

type User struct {
	Username string `json:"username" bson:"username"`
	Password string `json:"password" bson:"-"`
	HashedPassword []byte `json:"-" bson:"password"`
	db *mgo.Database
	session *mgo.Session
	c *mgo.Collection
}

//Is authorized if the username is in the db
func (u *User)IsAuthorized() bool {
	err := u.getDB()
	if err != nil {
		return false
	}
	u2 := &User{}
	err = u.c.Find(bson.M{"username": u.Username}).One(u2)
	if err != nil {
		return false
	}
	return len(u.Username) != 0 && u2.Username == u.Username
}

func (u *User)Save() error {
	if len(u.Password) < 8 {
		return errors.New("password is too short")
	}
	u.hashPassword()
	err := u.getDB()
	if err != nil {
		return err
	}
	_, err = u.c.Upsert(bson.M{"username": u.Username}, u)
	return err
}

func (u *User)Delete() error {
	return nil
}

func (u *User)CheckPassword() (bool, error) {
	err := u.getHashedPassword()
	if err != nil {
		return false, err
	}
	isGood:= bcrypt.CompareHashAndPassword(u.HashedPassword, []byte(u.Password))
	return isGood == nil, err
}

func (u *User)hashPassword() {
	u.HashedPassword, _ = bcrypt.GenerateFromPassword(
		[]byte(u.Password),
		bcrypt.DefaultCost,
	)
}

func (u *User)getHashedPassword() error {
	u2 := &User{}
	err := u.getDB()
	if err != nil {
		return err
	}
	defer u.Close()
	if err = u.c.Find(bson.M{"username": u.Username}).One(u2); err == nil {
		u.HashedPassword = u2.HashedPassword
	}
	return err
}

func (u *User)Close() {
	if u.session != nil {
		u.session.Close()
	}
}

func (u *User)getDB() error {
	if u.session != nil {
		return nil
	}
	var err error
	u.session, u.db, err = GetDB()
	if err == nil {
		u.c = u.db.C("users")
	}
	return err
}


