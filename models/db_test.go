package models

import (
	"testing"
)

func TestGetDB(t *testing.T) {
	session, _, err := GetDB()
	if err != nil {
		t.Error(err)
	}
	defer session.Close()
}
