package models

import (
	"gopkg.in/mgo.v2"
	"os"
)

func GetDB() (*mgo.Session, *mgo.Database, error) {
	host := getHost()
	session, err := mgo.Dial(host)
        if err != nil {
                return nil, nil, err
        }
        db := session.DB(getName())
	return session, db, nil
}

func getHost() string {
	conn := os.Getenv("PICS_DB_HOST")
	if conn == "" {
		conn = "localhost"
	}
	return conn
}

func getName() string {
	conn := os.Getenv("PICS_DB_NAME")
	if conn == "" {
		conn = "pics"
	}
	return conn
}
