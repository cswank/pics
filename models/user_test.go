package models

import (
	"testing"
	"os"
)

func TestSaveUser(t *testing.T) {
	os.Setenv("PICS_DB_NAME", "picstest")
	u := &User{Username: "craig", Password: "mygoodpassword"}
	err := u.Save()
	if err != nil {
		t.Error(err)
	}
	if !u.IsAuthorized() {
		t.Error("should have been authorized")
	}

	u.Username = "laura"
	if u.IsAuthorized() {
		t.Error("should not have been authorized")
	}
	u.Close()
}

