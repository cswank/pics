package models

import (
	"testing"
	"os"
)

func TestSavePicture(t *testing.T) {
	os.Setenv("PICS_DB_NAME", "picstest")
	f, _ := os.Open("test_data/test_pic.jpeg")
	p, err := NewPicture(f, "person.jpeg")
	if err != nil {
		t.Fatal(p, err)
	}
	err = p.Save()
	if err != nil {
		t.Fatal(p, err)
	}
}
