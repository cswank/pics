import requests
import json

HOST = "http://localhost"

h = {'content-type': 'application/json'}

def test_pics():
    data = json.dumps({'username': 'craig', 'password': 't00k3yP00@'})
    r = requests.post('{0}/api/login'.format(HOST), data=data, headers=h, verify=True)
    c = r.cookies
    print 'login', r.status_code, c, r.content

    r = requests.get('{0}/api/pics'.format(HOST), headers=h, verify=True)
    print 'get with no cookie', r.status_code

    r = requests.get('{0}/api/pics'.format(HOST), headers=h, verify=True, cookies=c)
    print 'get with cookie', r.status_code

    response = json.loads(r.content)
    print response
    for key, val in response["pictures"]:
        print key, val
